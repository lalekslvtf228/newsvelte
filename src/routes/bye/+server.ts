import { CookieSession } from 'svelte-kit-cookie-session/core';
import { json, type RequestHandler, redirect, fail } from '@sveltejs/kit';
import { getCookieValue, getCookieMaxage, initialData, SECRET } from '$lib/utils.ts';

export const GET: RequestHandler = async (event, locals) => {
    await event.locals.session.set(initialData)

    throw redirect(303, `/quiz/1`);
};