import { fail, error, redirect } from '@sveltejs/kit'


/** @type {import("./$types").PageServerLoad} */
export function load({ cookies, locals }) {
    console.log('locals.user', locals.user)
    let currstate = cookies.get('currstate')
    let randnum = Math.floor(Math.random() * 100);
    let coockie_obj = { randnum: randnum, attempt: 0 }

    if (!currstate) {
        cookies.set('currstate', JSON.stringify(coockie_obj))
    } else {
        let objstate = JSON.parse(currstate)
        coockie_obj.attempt = objstate.attempt
        cookies.set('currstate', JSON.stringify(coockie_obj))
    }

    /**{Object.<string, number>} */
    return coockie_obj
}

/**
 * @type {import("./$types").Actions}
 */
export const actions = {
    check: async ({ cookies, request, url }) => {
        const data = await request.formData()
        let answer = data.get('answer')

        let coockie_data = JSON.parse(cookies.get('currstate'))

        if (!answer) {
            return fail(400, { empty: true });
        }

        if (answer != coockie_data.randnum) {
            if (coockie_data.attempt === 3) {
                let redir_route = url.searchParams.get('/check')
                cookies.delete('currstate')
                //throw redirect(303, redir_route)
                return { attempt_three: true }
            } else {
                coockie_data.attempt += 1
                cookies.set('currstate', JSON.stringify(coockie_data))
                return { somedata: 'this is some data' }
            }
        } else {
            cookies.delete('currstate')
            throw redirect(303, '/')
        }
    }
}
