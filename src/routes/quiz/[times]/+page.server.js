//!!!!!!!!! https://www.npmjs.com/package/svelte-kit-cookie-session
//!!!!!!!!! https://github.com/pixelmund/svelte-kit-cookie-session#setting-the-session
//!!!!!!!!!!https://github.com/sveltejs/kit/discussions/5883

export const load = ({ url, cookies, request, locals }) => {

    const { data1 = [] } = locals.session.data;

    let cook = cookies.get('session_cookie')
    console.log("session_cookie", cook)

    return { special: { a: 123, b: 456 }, matter: ["a", "b", "c"] }

    //let u = new URL(request.url);
    //http://127.0.0.1:5173/quiz/0/__data.json?x-sveltekit-invalidated=_1
    // let cook_adder = cookies.get('adder')
    // if (!cook_adder) {
    //     cookies.set("adder", 0)
    // } else {//форма не отправлялась - перезагрузка
    //     if (!new URL(request.url).search) {
    //         cookies.set("adder", 0)
    //         return { summa: 0 }
    //     }
    //     return { summa: parseInt(cook_adder) }
    // }

};



/**@type {import('./$types').Actions} */
export const actions = {
    check: async ({ locals, cookies, url, request, params }) => {

        const { data1 = [] } = locals.session.data;
        // await locals.session.set({ views: 222, data1: [], data2: [] });

        const data = await request.formData()
        let answer = data.get("answer")

        //стандартная робота с cookis
        let cook_adder = cookies.get('adder')
        cookies.set('adder', (parseInt(answer) + parseInt(cook_adder)).toString(10))

        //работа с состоянием через url-параметры
        let summ = url.searchParams.get('/check')
        let res = parseInt(summ) + parseInt(answer);


        if (parseInt(answer) == 100) {
            const { data1 = [] } = locals.session.data;
            if (data1.length == 0) {
                await locals.session.set({ views: 222, data1: [-321], data2: [] });
            } else {
                await locals.session.update((data) => ({ data1: [...data.data1, 5, 6, 7] }))

                //BAD!! await locals.session.update(({ data1 }) => [...data1, 333, 444])
            }
        }


        try {
            if (parseInt(answer) == 200) {
                await locals.session.update((data) => ({
                    user: { name: "qq", email: "qq@ww.com" },
                    data2: [...data.data2, 'qq', 'ww', 'ee'],
                    data1: [...data.data1, 555, 666, 777],
                }));
            }
        } catch (err) {
            console.log(err)
        }
        await locals.session.refresh(10)

        return { summa: 1000, text: '<h1>ТЕКСТ</h1>' }
    }
}