import { handleSession } from 'svelte-kit-cookie-session';

//https://joyofcode.xyz/sveltekit-hooks


/** @type {import('@sveltejs/kit').Handle} */
export const handle = handleSession(
    {
        secret: '11111111111111111111111111111111',
        key: "session_cookie",
        expires: 1,
        cookie: {
            httpOnly: true, path: "/quiz"
        }

    },
    ({ event, resolve }) => {
        // event.locals is populated with the session `event.locals.session`
        // Do anything you want here

        console.log('hook.server.js->locals.session.data', event.locals.session.data)

        return resolve(event);
    }
);

