# ToDo


Построить приложение аналогичное https://learn.svelte.dev. На его основе:

## Цели

1.0. Разобраться с псевдонимом $lib и соответствующей папкой. - OK. alias $lib и папку нужно делать вручную.

1.1. Разобраться с динамическими параметрами [slug] и вложенностью



1. Разобраться с типизацией (TS - JSDoc)
Ресурсы: https://medium.com/@trukrs/type-safe-javascript-with-jsdoc-7a2a63209b76



2. Разобраться с функцией load по https://www.okupter.com/blog/sveltekit-internals-load-function 
Ресурсы: https://dev.to/sschuchlenz/how-the-page-load-function-works-in-svelte-sveltekit-328h



3. Разобраться с отладкой по https://meetupfeed.io/talk/debugging-svelte-kit-endpoints-and-load-function-using-the-debugger
4. Установить и изучить официальное демо-приложение



## Полезные ресурсы

1. https://github.com/sw-yx/swyxkit/  (A lightly opinionated starter for SvelteKit blogs)
   там же https://www.swyx.io/ideas?filter=sveltekit




### ИНТЕРЕСНО (вернуться)
Renderless Svelte Components: https://github.com/stephane-vanraes/renderless-svelte

### На будущее:
Разобраться с файловой системой NodeJS  https://gyandeeps.com/json-file-write/


# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
